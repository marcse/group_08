# Group_08

Project for Advanced Programming course. 
Topic: energy mix of several countries


## Name
Group_08

## Description
Group project for Advanced Programming for Data Science. 
Scenario: We are participating in a two-day hackathon promoted to study the energy mix of several countries. Goal is to analyse the energy mix of several countries.
For this project, we will be using data from Our World in Data.
Language: English
Programming language: Python





## Support
For more information contact 51262@novasbe.pt

## Roadmap
The project is divided into several phases and deadlines:  
**Day 1, Phase 1**  

One of you will create a gitlab repository (it does not matter who). THE NAME OF THE REPOSITORY MUST BE "Group_XX" where XX is the number of your group! If you are group 3, then XX must be 03. Always use two digits!
 Initialize the repo with a README.md file, a proper license, and a .gitignore for the programming language you will use.
 The one who created the repository will then give Maintainer permissions to the rest of the group. Check under "Project Information" > "Members".
 Every element of the group clones the repository to their own laptops.
 
 **Day1, Phase 2**  
The class you decide the create for the project has finally been named after a brief fight and is PEP8 compliant, like the entire project.
The class will have several methods, which you will not develop in the master branch.
Document everything!
Make your calls compliant with Static Type Checking.
One method will download the data file into a downloads/ directory in the root directory of the project (main project directory). If the data file already exists, the method will not download it again.
 This method must also read the dataset into a pandas dataframe which is an attribute of your class. Consider only years after 1970, inclusively.
 
**Day 1, Phase 3**  
Develop a second method that outputs a list of the available countries in the data set.
 Develop a third method that plots an area chart of the "_consumption" columns. This method should have two arguments: a country argument and a normalize argument. The latter normalizes the consumption in relative terms: each year, consumption should always be 100%.
 The latter method should return a ValueError when the chosen country does not exist.
 Develop a fourth method that may receive a string with a country or a list of country strings. This method should compare the total of the "_consumption" columns for each of the chosen countries and plot it, so a comparison can be made.
 Develop a fifth method that may receive a string with a country or a list of country strings. This method should compare the "gdp" column of each country over the years.
 Develop a sixth method that must be called gapminder. This is a reference to the famous gapminder tools. This method should receive an argument year which must be an int. If the received argument is not an int, the method should raise a TypeError. This method should plot a scatter plot where x is gdp, y is total energy consumption, and the area of each dot is population.  

**Day 1, Phase 4**  
 Make a "showcase notebook" where you import your Class and showcase all the methods you developed. Tell a story about your analysis and findings in the showcase notebook:
Let's begin by telling a story.
Start by an overall analysis with the gapminder plot for the most recent year and the year 1990. What can you see as the major changes?
Choose three countries in the list. Any countries will do, but at least one must be a member of the EU. The EU has a carbon tax, which means carbon emmissions have a cost. This will be useful for later on.
Use your methods to analyse the evolution of each country's energy mix. Describe briefly what the evolution of the mix is, both totally and relatively.
Check GDP evolution for each country you selected. Make a brief analysis for each country.
Check GDP total energy consumption for each country you selected. Make a brief analysis for each country.
Finish day one with a short overall analysis between energy consumption, population, and GDP. 

**Day 2, TBD** 

**Deadlines:**  
Day 1: February 25th, 2022  
Day 2: March 17th, 2022



## Authors and acknowledgment
Ana Catarina Velez Mendes  
Constança Da Cunha Dias Metelo Reis  
Konstantin Klettke  
Marc Philipp Seeger  

## License
GNU

## Project status
Day 1, Phase 1 almost completed
